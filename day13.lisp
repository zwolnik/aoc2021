(ql:quickload 'cl-utilities)

(defun read-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun parse-input (input)
  (loop for line in input
        if (find #\, line)
          collect (mapcar #'parse-integer (cl-utilities:split-sequence #\, line))
            into points
        if (find #\= line)
          collect (let ((last (car (last (cl-utilities:split-sequence #\Space line)))))
                    (destructuring-bind (axis value) (cl-utilities:split-sequence #\= last)
                      `(,axis ,(parse-integer value))))
            into folds
        finally (return `(,points ,folds))))

(defun fold (var val)
  (if (<= var val) var
      (let ((distance (- var val)))
        (- val distance))))

(defun fold-2d (point fold-info)
  (destructuring-bind (axis value) fold-info
    (cond ((equal "x" axis) `(,(fold (first point) value) ,(second point)))
          ((equal "y" axis) `(,(first point) ,(fold (second point) value))))))

(defun apply-folds (points folds)
  (loop for fold in folds
        do (setf points (remove-duplicates
                         (mapcar (lambda (point) (fold-2d point fold)) points)
                         :test #'equal)))
  points)

(let ((data (parse-input (read-file "day13.input"))))
  (destructuring-bind (points folds) data
    (format t "Part 1: ~a~%" (length (apply-folds points `(,(car folds)))))))

(defun print-code (points)
  (destructuring-bind (max-x max-y) (loop for (x y) in points
                                          maximize x into max-x
                                          maximize y into max-y
                                          finally (return `(,max-x ,max-y)))
    (loop for y from 0 to max-y
          do (format t "~{~A~}~%" (loop for x from 0 to max-x
                                      collect (if (member `(,x ,y) points :test #'equal)
                                                  #\#
                                                  #\Space))))))

(let ((data (parse-input (read-file "day13.input"))))
  (destructuring-bind (points folds) data
    (print-code (apply-folds points folds))))

(defvar *test-input* '("6,10"
                       "0,14"
                       "9,10"
                       "0,3"
                       "10,4"
                       "4,11"
                       "6,0"
                       "6,12"
                       "4,1"
                       "0,13"
                       "10,12"
                       "3,4"
                       "3,0"
                       "8,4"
                       "1,10"
                       "2,14"
                       "8,10"
                       "9,0"
                       ""
                       "fold along y=7"
                       "fold along x=5"))
(defvar *test-data* (parse-input *test-input*))
(destructuring-bind (points folds) *test-data*
  (assert (= 16 (length (apply-folds points folds)))))
