(ql:quickload 'cl-utilities)

(defun read-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun parse-input (input)
  (mapcar (lambda (line) (cl-utilities:split-sequence #\- line)) input))

(defun cave-system (data)
  (let ((system (make-hash-table :test #'equal)))
    (loop for (source dest) in data
          do (setf (gethash source system) (push dest (gethash source system)))
          do (setf (gethash dest system) (push source (gethash dest system))))
    system))

(defun big-cave-p (cave)
  (every #'upper-case-p cave))

(defun travel (cave path system add-path)
  (let ((path (push cave path)))
    (funcall add-path path)
    (unless (equal "end" cave)
      (let* ((next (gethash cave system))
             (next (remove-if (lambda (cave) (and (member cave path :test #'equal)
                                                  (not (big-cave-p cave)))) next)))
        (loop for cave in next
              do (travel cave path system add-path))))))

(defun find-paths (system)
  (let* ((paths nil)
         (add-path (lambda (path) (push path paths))))
    (travel "start" nil system add-path)
    (remove-if-not (lambda (path) (equal "end" (car path))) paths)))

(let ((data (parse-input (read-file "day12.input"))))
  (format t "Part 1: ~a~%" (length (find-paths (cave-system data)))))

(defun travel+ (cave path system doubling add-path)
  (let ((path (push cave path)))
    (funcall add-path path)
    (unless (equal "end" cave)
      (let* ((neighbours (gethash cave system))
             (next (remove-if (lambda (cave) (and (member cave path :test #'equal)
                                                  (not (big-cave-p cave)))) neighbours)))
        (cond ((not doubling)
               (when (and (not (big-cave-p cave)) (not (equal "start" cave)))
                 (loop for c in next
                       do (travel+ c path system `(,cave) add-path))))
              ((= 1 (length doubling))
               (let ((doubled (car doubling)))
                 (when (member doubled neighbours :test #'equal)
                   (travel+ doubled path system `(,doubled ,doubled) add-path)))))
        (loop for cave in next
              do (travel+ cave path system doubling add-path))))))

(defun find-paths+ (system)
  (let* ((paths nil)
         (add-path (lambda (path) (push path paths))))
    (travel+ "start" nil system nil add-path)
    (remove-duplicates (remove-if-not (lambda (path) (equal "end" (car path))) paths)
                       :test #'equal)))

(let ((data (parse-input (read-file "day12.input"))))
  (format t "Part 2: ~a~%" (length (find-paths+ (cave-system data)))))

(defvar *test-input* '("start-A"
                       "start-b"
                       "A-c"
                       "A-b"
                       "b-d"
                       "A-end"
                       "b-end"))
(defvar *test-data* (parse-input *test-input*))
(assert (= 10 (length (find-paths (cave-system *test-data*)))))
(assert (= 36 (length (find-paths+ (cave-system *test-data*)))))
(format t "Tests passed")
