(ql:quickload '(cl-utilities alexandria))

(defun read-input (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun parse-line (line)
  (mapcar (lambda (part) (cl-utilities:split-sequence #\Space (string-trim " " part)))
          (cl-utilities:split-sequence #\| line)))

(defun parse-input (input)
  (mapcar #'parse-line input))

(defun is-known-digit (digit)
  (let ((len (length digit)))
    (or (= 7 len) (<= len 4))))

(defun count-known-digits (input)
  (loop for digits in input
        for output = (second digits)
        sum (loop for digit in output
                  count (is-known-digit digit))))

(format t "part1: ~a~%" (count-known-digits (parse-input (read-input "day8.input"))))

(defun make-solution ()
  (let ((solution (make-hash-table :test #'equal)))
    (loop for letter across "abcdefg"
          do (setf (gethash letter solution) '(1 2 3 4 5 6 7)))
    solution))

(defun print-sol (solution)
  (loop for v being each hash-values of solution using (hash-key k)
        do (format t "~a => ~a~%" k v)))

(defun sol-equal (sol1 sol2)
  (equal (alexandria:hash-table-plist sol1) (alexandria:hash-table-plist sol2)))

(defun map-key (solution key f)
  (let* ((current (gethash key solution))
         (next (funcall f current)))
    (setf (gethash key solution) next)))

(defun keep (solution key values)
  (map-key solution key
           (lambda (current) (remove-if-not (lambda (elem) (member elem values)) current))))

(defun remove-vals (solution key values)
  (map-key solution key
           (lambda (current) (remove-if (lambda (elem) (member elem values)) current))))

(defun apply-unique-length (line solution)
  (loop for word in (apply #'append line)
        for len = (length word)
        do (cond ((= 2 len) (loop for key across word
                                  do (keep solution key '(2 3))))
                 ((= 3 len) (loop for key across word
                                  do (keep solution key '(1 2 3))))
                 ((= 4 len) (loop for key across word
                                  do (keep solution key '(2 3 6 7)))))))

(defun group-by-value (solution)
  (let ((same (make-hash-table :test #'equal)))
    (loop for val being each hash-values of solution using (hash-key key)
          do (loop for v being each hash-values of solution using (hash-key k)
                   do (when (equal val v)
                        (setf (gethash val same) (adjoin key (gethash val same)))
                        (setf (gethash val same) (adjoin k (gethash val same))))))
    same))

(defun remove-finite-sets (solution)
  (let ((same (group-by-value solution))
        (result (alexandria:copy-hash-table solution)))
    (loop for keys being each hash-values of same using (hash-key vals)
          if (= (length keys) (length vals))
            do (loop for v being each hash-values of solution using (hash-key k)
                     if (not (member k keys))
                       do (remove-vals result k vals)))
    result))

(defvar *num-to-segments* (let ((map (make-hash-table)))
                            (setf (gethash 0 map) '(1 2 3 4 5 6))
                            (setf (gethash 1 map) '(2 3))
                            (setf (gethash 2 map) '(1 2 4 5 7))
                            (setf (gethash 3 map) '(1 2 3 4 7))
                            (setf (gethash 4 map) '(2 3 6 7))
                            (setf (gethash 5 map) '(1 3 4 6 7))
                            (setf (gethash 6 map) '(1 3 4 5 6 7))
                            (setf (gethash 7 map) '(1 2 3))
                            (setf (gethash 8 map) '(1 2 3 4 5 6 7))
                            (setf (gethash 9 map) '(1 2 3 4 6 7))
                            map))

(defvar *segments* '(#\a #\b #\c #\d #\e #\f #\g))

(defun len5 (seq)
  (= 5 (length seq)))

(defun len6 (seq)
  (= 6 (length seq)))

(defun determine-by-six-len (input solution)
  (let ((input (apply #'append input))
        (get-missing (lambda (segments) (loop for seg in *segments*
                                              if (not (find seg segments))
                                                return seg))))
    (loop for segments in (remove-if-not #'len6 input)
          for missing = (funcall get-missing segments)
          do (loop for current in (gethash missing solution)
                   if (member current '(2 5 7))
                     do (keep solution missing '(2 5 7))))))

(defun num-from-digits (digits)
  (loop for d in (nreverse digits)
        for x from 0
        sum (* d (expt 10 x))))

(defun segments-to-num (segs)
  (or (loop for val being each hash-values of *num-to-segments* using (hash-key key)
            with sorted-segs = (sort segs #'<)
            if (equal val sorted-segs)
              return key)
      (assert nil)))

(defun solve (line)
  (let ((solution (make-solution)))
    (apply-unique-length line solution)

    (loop for result = (remove-finite-sets solution)
          while (not (sol-equal result solution))
          do (setf solution result))

    (determine-by-six-len line solution)

    (loop for result = (remove-finite-sets solution)
          while (not (sol-equal result solution))
          do (setf solution result))

    (assert (every (lambda (num) (= 1 num)) (mapcar #'length (alexandria:hash-table-values solution))))

    (num-from-digits (loop for output in (second line)
                           collect (segments-to-num (loop for char across output
                                                          collect (car (gethash char solution))))))))

(defun solve-batch (input)
  (reduce #'+ (mapcar #'solve input)))

(format t "part 2: ~a~%" (solve-batch (parse-input (read-input "day8.input"))))

(defvar *test-data* '("be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
                      "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc"
                      "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg"
                      "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb"
                      "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea"
                      "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb"
                      "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe"
                      "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef"
                      "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb"
                      "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"))

(defvar *test-input* (parse-input *test-data*))

(defun test ()
  (assert (= 26 (count-known-digits *test-input*)))
  (assert (= 61229 (solve-batch *test-input*))))
(test)
