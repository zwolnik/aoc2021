(ql:quickload '(cl-utilities alexandria))

(defun read-input (filename)
  (with-open-file (stream filename) (read-line stream nil)))

(defun parse-input (input)
  (mapcar #'parse-integer (cl-utilities:split-sequence #\, input)))

(defun test (f)
  (let ((input "3,4,3,1,2"))
    (assert (= 5934 (funcall f 80 (parse-input input))))))

; part 1
(defun next-day (lanternfish)
  (alexandria:flatten (mapcar (lambda (fish) (if (= 0 fish) '(8 6) `(,(1- fish)))) lanternfish)))

(defun simulation (days lanternfish)
  (if (= 0 days) lanternfish (simulation (1- days) (next-day lanternfish))))

(test (lambda (days lanternfish) (length (simulation days lanternfish))))
(format t "Part1 result: ~a~%"
        (length (simulation 80 (parse-input (read-input "day6.input")))))

(defun calculate-population (days lanternfish)
  (reduce #'+ (mapcar (lambda (fish) (calculate-lanternfish days fish)) lanternfish)))

(test #'calculate-population)
(format t "Part2 result: ~a~%"
        (calculate-population 256 (parse-input (read-input "day6.input")))) ; 256 unreachable ; reachable after 24h :)
