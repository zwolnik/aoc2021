(ql:quickload '(str alexandria))

(defun read-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect line)))

(defun parse-input (input)
  (let ((template (car input))
        (patterns (cddr input))
        (insertions (make-hash-table :test #'equal)))
    (loop for entry in patterns
          for (pattern sep insertion) = (str:split #\Space entry)
          do (setf (gethash pattern insertions) insertion))
    `(,template ,insertions)))

(defun assemble (template insertions)
  (loop with result = ""
        for x from 0 to (- (length template) 2)
        for key = (subseq template x (+ 2 x))
        for insertion = (gethash key insertions)
        for cur = (if insertion
                      (str:concat (str:s-first key) insertion)
                      (str:s-first key))
        do (setf result (str:concat result cur))
        finally (return (str:concat result (str:s-last template)))))

(defun nassemble (steps data)
  (destructuring-bind (template insertions) data
    (loop for result = template then (assemble result insertions)
          for n from 1 to steps
          finally (return result))))

(defun count-chars (template)
  (loop with result = (make-hash-table :test #'equal)
        for ch across template
        do (incf (gethash ch result 0))
        finally (return result)))

(defun minmax (ch-counts)
  (let ((values (alexandria:hash-table-values ch-counts)))
    `(,(apply #'min values) ,(apply #'max values))))

(let ((template (nassemble 10 (parse-input (read-file "day14.input")))))
  (destructuring-bind (min max) (minmax (count-chars template))
    (format t "part1: ~a~%" (- max min))))

(defun nassemble-conc (steps data threads)
  (destructuring-bind (template insertions) data
    (let* ((part-len (floor (/ (length template) threads)))
           (handles (loop for n from 0 to (1- threads)
                          for sleep = (sleep 0.1)
                          for last = (= n (1- threads))
                          for start = (* n part-len)
                          for end = (if last t (1+ (* (1+ n) part-len))) ; 1+ to include additional ending
                          for subtempl = (str:substring start end template)
                          collect (sb-thread:make-thread
                                   (lambda () (nassemble steps `(,subtempl ,insertions)))))))
      (loop for handle in handles
            for value = (sb-thread:join-thread handle)
            collect (str:substring 0 -1 value) into parts
            finally (return (str:concat (apply #'str:concat parts) (str:s-last template)))))))

; (let ((template (nassemble-conc 10 (parse-input (read-file "day14.input")) 4)))
;   (destructuring-bind (min max) (minmax (count-chars template))
;     (format t "part2 conc: ~a~%" (- max min))))

(defun make-solution (data)
  (let ((solution (alexandria:copy-hash-table data :test #'equal)))
    (loop for key being each hash-key of solution
          do (setf (gethash key solution) 0))
    solution))

(defmacro inc-key (key sol)
  `(incf (gethash ,key ,sol)))

(defmacro add-to-key (key val sol)
  `(setf (gethash ,key ,sol) (+ (gethash ,key ,sol 0) ,val)))

(defun prepare (template insertions)
  (let ((solution (make-solution insertions)))
    (loop for n from 0 to (- (length template) 2)
          for m from 2 to (length template)
          do (inc-key (str:substring n m template) solution))
    solution))

(defun print-sol (solution)
  (mapcar (lambda (pair) (format t "~a => ~a~%" (car pair) (cdr pair)))
          (alexandria:hash-table-alist solution))
  nil)

(defun next-step (insertions solution)
  (let ((next (make-solution insertions)))
    (loop for v being each hash-values of solution using (hash-key k)
          for fst = (str:concat (str:s-first k) (gethash k insertions))
          for snd = (str:concat (gethash k insertions) (str:s-last k))
          do (add-to-key fst v next)
          do (add-to-key snd v next))
    next))

(defun prognose (days data)
  (destructuring-bind (template insertions) data
    (let ((solution (prepare template insertions)))
      (loop for n from 0 to days
            for sol = solution then (next-step insertions sol)
            finally (return sol)))))

(defun count-chars-sol (solution)
  (loop with result = (make-hash-table :test #'equal)
        for v being each hash-values of solution using (hash-key k)
        do (add-to-key (str:s-first k) v result)
        do (add-to-key (str:s-last k) v result)
        finally (return (loop for v being each hash-values of result using (hash-key k)
                              do (setf (gethash k result) (ceiling (/ v 2)))
                              finally (return result)))))

(let ((solution (prognose 40 (parse-input (read-file "day14.input")))))
  (destructuring-bind (min max) (minmax (count-chars-sol solution))
    (format t "part2: ~a~%" (- max min))))

(defvar *test-input* '("NNCB"
                       ""
                       "CH -> B"
                       "HH -> N"
                       "CB -> H"
                       "NH -> C"
                       "HB -> C"
                       "HC -> B"
                       "HN -> C"
                       "NN -> C"
                       "BH -> H"
                       "NC -> B"
                       "NB -> B"
                       "BN -> B"
                       "BB -> N"
                       "BC -> B"
                       "CC -> N"
                       "CN -> C"))
(defvar *test-data* (parse-input *test-input*))
(format t "~a~%" (count-chars (nassemble 4 *test-data*)))
