(defun read-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun parse-input (input)
  (loop for row in input
        collect (loop for ch across row
                      collect ch)))

(defvar +openings+ '(#\( #\[ #\< #\{))
(defvar +closings+ '(#\) #\] #\> #\}))

(defun corresponding (ch)
  (cond ((eql #\( ch) #\))
        ((eql #\[ ch) #\])
        ((eql #\< ch) #\>)
        ((eql #\{ ch) #\})))

(defun checker (line)
  (let* ((stack nil))
    (or (loop for ch in line
              if (member ch +openings+)
                do (push ch stack)
              if (member ch +closings+)
                if (not (eql ch (corresponding (pop stack))))
                  return ch)
        stack)))

(defun score (ch)
  (cond ((eql #\) ch) 3)
        ((eql #\] ch) 57)
        ((eql #\> ch) 25137)
        ((eql #\} ch) 1197)))

(defun blame-score (data)
  (loop for row in data
        for invalid = (checker row)
        if (characterp invalid)
          sum (score invalid)))

(format t "Part 1: ~a~%" (blame-score (parse-input (read-file "day10.input"))))

(defun score+ (ch)
  (cond ((eql #\) ch) 1)
        ((eql #\] ch) 2)
        ((eql #\> ch) 4)
        ((eql #\} ch) 3)))

(defun blame-score+ (data)
  (let* ((scores (loop for row in data
                       for invalid = (checker row)
                       if (not (characterp invalid))
                         collect (loop for ch in invalid
                                       with total = 0
                                       do (progn (setf total (* 5 total))
                                                 (setf total (+ total (score+ (corresponding ch)))))
                                       finally (return total))))
         (sorted (sort scores #'<)))
    (nth (floor (/ (length sorted) 2)) sorted)))

(format t "Part 2: ~a~%" (blame-score+ (parse-input (read-file "day10.input"))))

(defvar *test-input* '("[({(<(())[]>[[{[]{<()<>>"
                       "[(()[<>])]({[<{<<[]>>("
                       "{([(<{}[<>[]}>{[]{[(<()>"
                       "(((({<>}<{<{<>}{[]{[]{}"
                       "[[<[([]))<([[{}[[()]]]"
                       "[{[{({}]{}}([{[{{{}}([]"
                       "{<[[]]>}<{[{[{[]{()[[[]"
                       "[<(<(<(<{}))><([]([]()"
                       "<{([([[(<>()){}]>(<<{{"
                       "<{([{{}}[<[[[<>{}]]]>[]]"))
(defvar *test-data* (parse-input *test-input*))
(assert (= 26397 (blame-score *test-data*)))
(assert (= 288957 (blame-score+ *test-data*)))
