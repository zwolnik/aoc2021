(ql:quickload 'cl-utilities)

(defun is-marked (cell)
  (getf cell :marked))

(defun get-value (cell)
  (getf cell :value))

(defun parse-sequence (seq)
  (mapcar #'parse-integer (cl-utilities:split-sequence #\, seq)))

(defun parse-bingo-row (line)
  (mapcar (lambda (cell) `(:marked nil :value ,(parse-integer cell)))
          (remove-if (lambda (str) (equal "" str))
                     (cl-utilities:split-sequence #\Space line))))

(defun parse-bingos (input &key (size 5))
  (let ((input (remove-if (lambda (line) (equal "" line)) input)))
    (loop for line in input
          for n = 0 then (mod (1+ n) size)
          for bingos = (progn
                         (when (= n 0) (push nil bingos))
                         (push (parse-bingo-row line) (car bingos))
                         bingos)
          finally (return bingos))))

(defun parse-input (input)
  (let* ((sequence (parse-sequence (car input)))
         (bingos (parse-bingos (cdr input))))
    `(:sequence ,sequence :bingos ,bingos)))

(defun has-marked-row (bingo)
  (loop for row in bingo
          thereis (loop for cell in row
                        always (is-marked cell))))

(defun check-bingo (bingo)
  (let ((transposed-bingo (apply #'mapcar #'list bingo)))
    (or (has-marked-row bingo)
        (has-marked-row transposed-bingo))))

(defun find-winner (bingos)
  (loop for bingo in bingos
        for n from 0
        for winner = (if (check-bingo bingo) n nil)
        while (not winner)
        finally (return winner)))

(defun next-draw (number bingos)
  (loop for bingo in bingos
        do (loop for row in bingo
                 do (loop for cell in row
                          do (when (= number (get-value cell))
                               (setf (getf cell :marked) t))))))

(defun eval-board (bingo)
  (loop for row in bingo
        sum (loop for cell in row
                  sum (if (is-marked cell) 0 (get-value cell)))))

(defun play (sequence bingos)
  (loop for number in sequence
        for winner = (progn (next-draw number bingos)
                            (find-winner bingos))
        while (not winner)
        finally (return (* number (eval-board (nth winner bingos))))))

(defun last-winning-bingo (sequence bingos)
  (let ((winners nil))
    (loop for number in sequence
          for boards = bingos then (remove-if #'check-bingo boards)
          do (progn (next-draw number boards)
                    (let ((winner (find-winner boards)))
                      (when (and winner (not (cdr boards)))
                        (return (* number (eval-board (car boards)))))))
          while boards)))

(defun read-input (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect line)))

(let* ((input (read-input "day4.input"))
       (sequence (parse-sequence (car input)))
       (bingos (parse-bingos (cdr input))))
  (format t "Part1 result: ~a~%" (play sequence bingos)))

(let* ((input (read-input "day4.input"))
       (sequence (parse-sequence (car input)))
       (bingos (parse-bingos (cdr input))))
  (format t "Part2 result: ~a~%" (last-winning-bingo sequence bingos)))

(defun test ()
  (let ((input '("7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1"

                 "22 13 17 11  0"
                 " 8  2 23  4 24"
                 "21  9 14 16  7"
                 " 6 10  3 18  5"
                 " 1 12 20 15 19"

                 " 3 15  0  2 22"
                 " 9 18 13 17  5"
                 "19  8  7 25 23"
                 "20 11 10 24  4"
                 "14 21 16 12  6"

                 "14 21 17 24  4"
                 "10 16 15  9 19"
                 "18  8 23 26 20"
                 "22 11 13  6  5"
                 " 2  0 12  3  7")))
    (let ((sequence (parse-sequence (car input)))
          (bingos (parse-bingos (cdr input))))
      (assert (= 4512 (bingo sequence bingos)))
      (assert (= 1924 (last-winning-bingo sequence bingos))))
    (format t "Test passed")))
(test)
