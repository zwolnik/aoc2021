(defun read-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun parse-line (line)
  (loop for num across line
        collect (digit-char-p num)))

(defun parse-input (input)
  (mapcar #'parse-line input))

(defun lowest (line)
  (let ((fst (first line))
        (snd (second line))
        (butlst (car (last (butlast line))))
        (lst (car (last line))))
    (append (when (< fst snd) '(0))
            (loop for cur in (cdr (butlast line))
                  for n from 1
                  for prev = (nth (1- n) line)
                  for next = (nth (1+ n) line)
                  if (and (> prev cur) (< cur next))
                    collect n)
            (when (> butlst lst) `(,(1- (length line)))))))

(defun minimums (data)
  (let ((horizontal (apply #'append (loop for row in data
                                          for y from 0
                                          collect (mapcar (lambda (x) `(,x ,y)) (lowest row)))))
        (vertical (apply #'append (loop for col from 0 to (1- (length (car data)))
                                        for x from 0
                                        collect (mapcar (lambda (y) `(,x ,y))
                                                        (lowest (mapcar (lambda (row) (nth col row)) data)))))))
    (remove-if-not (lambda (point) (member point horizontal :test #'equal)) vertical)))

(defun at (point data)
  (nth (first point) (nth (second point) data)))

(defun risk-level (data)
  (reduce #'+ (loop for point in (minimums data)
                    collect (1+ (at point data)))))

(format t "part1: ~a~%" (risk-level (parse-input (read-file "day9.input"))))

(defun neighbours (point width height)
  (loop for (xoff yoff) in '((1 0) (0 1) (-1 0) (0 -1))
        for nextx = (+ xoff (first point))
        for nexty = (+ yoff (second point))
        if (and (<= 0 nextx) (< nextx width)
                (<= 0 nexty) (< nexty height))
          collect `(,nextx ,nexty)))

(defun unvisited-neighbours (point width height visited)
  (remove-if (lambda (point) (member point visited :test #'equal))
             (neighbours point width height)))

(defun basins-impl (point width height visit visited data)
  (funcall visit point)
  (format t "current: ~a, visited: ~a~%" point (funcall visited))
  (loop for neighbour = (car (remove-if (lambda (point) (= 9 (at point data)))
                                        (unvisited-neighbours point width height (funcall visited))))
        while neighbour
        collect (progn (format t "neighbour ~a of ~a~%" neighbour point)
                       (basins-impl neighbour width height visit visited data))))

(defun basins (data)
  (let ((minims (minimums data))
        (width (length (car data)))
        (height (length data)))
    (loop for minim in minims
          do (format t "minimum ~a~%" minim)
          collect (let* ((visited-points nil)
                         (visit (lambda (point) (push point visited-points)))
                         (visited (lambda () visited-points)))
                    (basins-impl minim width height visit visited data)
                    visited-points))))

(defun basins-count (basins)
  (let* ((sorted (mapcar (lambda (basin)
                           (sort basin (lambda (lhs rhs) (destructuring-bind ((x1 y1) (x2 y2)) `(,lhs ,rhs)
                                                           (if (= x1 x2)
                                                               (< y1 y2)
                                                               (< x1 x2))))))
                         basins))
         (sorted (remove-duplicates sorted :test #'equal)))
    (reduce #'* (loop for basin in (sort sorted (lambda (l r) (> (length l) (length r))))
          for n from 1 to 3
          collect (length basin)))))

(format t "~a~%" (basins-count (basins (parse-input (read-file "day9.input")))))

(defvar *test-input* '("2199943210"
                      "3987894921"
                      "9856789892"
                      "8767896789"
                      "9899965678"))
(defvar *test-data* (parse-input *test-input*))
(assert (= 15 (risk-level *test-data*)))
