(defun parse-step (line)
  (let* ((value-pos (1+ (position #\  line)))
         (value (parse-integer line :start value-pos)))
    (cond ((search "forward" line) `(:forward ,value))
          ((search "down" line) `(:down ,value))
          ((search "up" line) `(:up ,value)))))

(defun cruise (steps)
  (loop for (key value) in steps
        if (equal :forward key)
          sum value into horizontal
        if (equal :down key)
          sum value into depth
        if (equal :up key)
          sum (- value) into depth
        finally (return (list horizontal depth))))

(defun cruise-with-aim (steps)
  (loop for (key value) in steps
        if (equal :forward key)
          sum value into horizontal
          and sum (* aim value) into depth
        if (equal :down key)
          sum value into aim
        if (equal :up key)
          sum (- value) into aim
        finally (return (list horizontal depth))))

(defun read-input (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect (parse-step line))))

(let* ((input (read-input "day2.input"))
       (result (cruise input)))
  (format t "Day2 part 1 result: ~a~%" (* (first result) (second result))))

(let* ((input (read-input "day2.input"))
       (result (cruise-with-aim input)))
  (format t "Day2 part 2 result: ~a~%" (* (first result) (second result))))
