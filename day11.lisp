(defvar +width+ 10)
(defvar +height+ 10)

(defun read-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun parse-line (line)
  (loop for num across line
        collect (digit-char-p num)))

(defun parse-input (input)
  (mapcar #'parse-line input))

(defmacro at (point data)
  `(nth (first ,point) (nth (second ,point) ,data)))

(defun neighbours (point)
  (loop for xoff in '(-1 0 1)
        nconc (loop for yoff in '(-1 0 1)
                    for nextx = (+ xoff (first point))
                    for nexty = (+ yoff (second point))
                    if (and (or (/= 0 xoff) (/= 0 yoff))
                            (<= 0 nextx) (< nextx +width+)
                            (<= 0 nexty) (< nexty +height+))
                      collect `(,nextx ,nexty))))

(defun unflashed (neighbours getpoint)
  (remove-if-not (lambda (point) (> 10 (funcall getpoint point))) neighbours))

(defun mapcell (data func)
  (loop for row in data
        for y from 0
        collect (loop for cell in row
                      for x from 0
                      collect (funcall func cell x y))))

(defun clear-flashed (data)
  (mapcell data (lambda (cell x y) (declare (ignore x)) (declare (ignore y))
                        (if (< 9 cell) 0 cell))))

(defun increment (data)
  (mapcell data (lambda (cell x y) (declare (ignore x)) (declare (ignore y))
                        (1+ cell))))

(defun flash-cell (point incpoint getpoint mark)
  (funcall mark point)
  (funcall incpoint point)
  (when (< 9 (funcall getpoint point))
    (loop for neighbour in (unflashed (neighbours point) getpoint)
          do (funcall incpoint neighbour))
    (loop for neighbour in (neighbours point)
          if (= 10 (funcall getpoint neighbour))
            do (flash-cell neighbour incpoint getpoint mark))))

(defun flash (data)
  (let* ((incpoint (lambda (point) (setf (at point data) (1+ (at point data)))))
         (getpoint (lambda (point) (at point data)))
         (flashed nil)
         (mark (lambda (point) (push point flashed)))
         (flashpoints nil))
    (mapcell data (lambda (cell x y) (when (= 10 cell) (push `(,x ,y) flashpoints))))
    (loop for point in flashpoints
          if (= 10 (at point data))
            do (flash-cell point incpoint getpoint mark))
    `(,flashed ,data)))

(defun next-step (data)
  (destructuring-bind (flashed data) (flash (increment data))
    `(,flashed ,(clear-flashed data))))

(defun flashed-after-n (days data)
  (loop for x from 1 to days
        for (flashed next-data) = (next-step data) then (next-step next-data)
        sum (length flashed)))

(format t "Part 1: ~a~%" (flashed-after-n 100 (parse-input (read-file "day11.input"))))

(defun first-synchronous-flash (data)
  (loop for x from 1
        for (flashed next-data) = (next-step data) then (next-step next-data)
        if (= (* +width+ +height+) (length flashed))
          return x))

(format t "Part 2: ~a~%" (first-synchronous-flash (parse-input (read-file "day11.input"))))

(defvar *test-input* '("5483143223"
                       "2745854711"
                       "5264556173"
                       "6141336146"
                       "6357385478"
                       "4167524645"
                       "2176841721"
                       "6882881134"
                       "4846848554"
                       "5283751526"))
(defvar *test-data* (parse-input *test-input*))
