(ql:quickload 'cl-utilities)

(defun points-table ()
  (make-hash-table :test #'equal))

(defun add-point (table point)
  (let ((current-val (gethash point table)))
    (if current-val
        (setf (gethash point table) (1+ current-val))
        (setf (gethash point table) 1))))

(defun as-string (point)
  (format nil "~a-~a" (get-x point) (get-y point)))

(defun get-x (point)
  (first point))

(defun get-y (point)
  (second point))

(defun parse-point (input)
  (destructuring-bind (x y)
      (cl-utilities:split-sequence #\, input)
    `(,(parse-integer x) ,(parse-integer y))))

(defun parse-segment (input)
  (destructuring-bind (fst _ snd)
      (cl-utilities:split-sequence #\Space input)
    (declare (ignore _))
    `(,(parse-point fst) ,(parse-point snd))))

(defun points-from-segment (segment)
  (destructuring-bind ((x1 y1) (x2 y2)) segment
    (cond ((= x1 x2)
           (loop with (lower-bound upper-bound) integer = (if (> y2 y1) `(,y1 ,y2) `(,y2 ,y1))
                 for y from lower-bound to upper-bound
                 collecting `(,x1 ,y)))
          ((= y1 y2)
           (loop with (lower-bound upper-bound) integer = (if (> x2 x1) `(,x1 ,x2) `(,x2 ,x1))
                 for x from lower-bound to upper-bound
                 collecting `(,x ,y1)))
                                        ;))) delete for part 1
          ((and (>= x2 x1) (>= y2 y1))
           (loop for x from x1 to x2
                 for y from y1 to y2
                 collecting `(,x ,y)))
          ((and (<= x2 x1) (<= y2 y1))
           (loop for x from x1 downto x2
                 for y from y1 downto y2
                 collecting `(,x ,y)))
          ((< x2 x1)
           (loop for x from x1 downto x2
                 for y from y1 to y2
                 collecting `(,x ,y)))
          ((< y2 y1)
           (loop for x from x1 to x2
                 for y from y1 downto y2
                 collecting `(,x ,y))))))

(defun read-input (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line collect line)))

(defun fill-table (input)
  (let ((points (points-table)))
    (loop for line in input
          do (loop for point in (points-from-segment (parse-segment line))
                   do (add-point points point)))
    points))

(defun count-dangerous (table)
  (loop for v being the hash-value in table
        count (> v 1)))

; main
(let ((points (fill-table (read-input "day5.input"))))
  (format t "Result: ~a~%" (count-dangerous points)))

(defun test ()
  (let* ((input '("0,9 -> 5,9"
                  "8,0 -> 0,8"
                  "9,4 -> 3,4"
                  "2,2 -> 2,1"
                  "7,0 -> 7,4"
                  "6,4 -> 2,0"
                  "0,9 -> 2,9"
                  "3,4 -> 1,4"
                  "0,0 -> 8,8"
                  "5,5 -> 8,2"))
         (points (fill-table input)))
    (assert (= 12 (count-dangerous points))) ; 5 for part 1
    (format t "Test passed")))
(test)
