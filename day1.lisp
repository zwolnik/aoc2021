(defun windows (list &key (size 2))
  (apply #'mapcar #'list
         (loop repeat size
               for cons on list
               collect cons)))

(defun count-increases (list)
  (loop for (l r) in (windows list)
        count (< l r)))

(defun compute-3-measurement-windows-sums (list)
  (loop for (first second third) in (windows list :size 3)
        collect (+ first second third)))

(defun read-input (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect (parse-integer line))))

(defun test-part-1 ()
  (let ((input '(199 ; N/A
                 200 ; +
                 208 ; +
                 210 ; +
                 200 ; -
                 207 ; +
                 240 ; +
                 269 ; +
                 260 ; -
                 263))) ; +
    (assert (= 7 (count-increases input)))
    (format t "Part 1 passed~%")))

(defun test-part-2 ()
  (let ((input '(607 ; N/A
                 618 ; +
                 618 ;
                 617 ; -
                 647 ; +
                 716 ; +
                 769 ; +
                 792))) ; +
    (assert (= 5 (count-increases input)))
    (format t "Part 2 passed~%")))

(test-part-1)
(let* ((input (read-input "day1.input"))
       (result (count-increases input)))
  (format t "Day1 part 1 result: ~a~%" result))

(test-part-2)
(let* ((input (read-input "day1.input"))
       (sums (compute-3-measurement-windows-sums input))
       (result (count-increases sums)))
  (format t "Day1 part 2 result: ~a~%" result))
