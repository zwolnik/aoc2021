(defun getbit (number index)
  (let* ((shift index)
         (mask (ash 1 shift)))
    (ash (logand number mask) (- shift))))

(defun get-gamma (report width)
  (loop for n from (- width 1) downto 0
        collecting (bit-by-criteria n report #'bit-criteria-oxygen)))

(defun epsilon-from-gamma (gamma)
  (mapcar (lambda (x) (- 1 x)) gamma))

(defun bits-to-num (bitlist)
   (reduce (lambda (a b) (+ (ash a 1) b)) bitlist))

(defun power-consumption (report width)
  (let* ((gamma (get-gamma report width))
         (epsilon (epsilon-from-gamma gamma)))
    (* (bits-to-num gamma) (bits-to-num epsilon))))

(defun bit-criteria-oxygen (zeros ones)
  (<= zeros ones))

(defun bit-criteria-co2 (zeros ones)
  (> zeros ones))

(defun bit-by-criteria (n list criteria)
  (loop for num in list
        for bit = (getbit num n)
        count (= 1 bit) into ones
        count (= 0 bit) into zeros
        finally (progn (return (if (funcall criteria zeros ones) 1 0)))))

(defun get-rating (report width bit-criteria)
  (let ((selected report))
    (loop while (cdr selected)
          for n from (1- width) downto 0
          for bit = (bit-by-criteria n selected bit-criteria)
          do (setf selected
                   (remove-if-not (lambda (elem) (= bit (getbit elem n)))
                                  selected))
          finally (return (car selected)))))

(defun read-input (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          maximize (length line) into width
          collect (parse-integer line :radix 2) into report
          finally (return `(,report ,width)))))

; part 1 solution
(format t "Part 1 result: ~a~%" (apply #'power-consumption (read-input "day3.input")))
; part 2 solution
(let* ((readed (read-input "day3.input"))
       (input (first readed))
       (width (second readed))
       (oxygen-rating (get-rating input width #'bit-criteria-oxygen))
       (co2-rating (get-rating input width #'bit-criteria-co2)))
  (format t "Part 2 result: ~a~%" (* oxygen-rating co2-rating)))

(defun test ()
  (let ((input '("00100"
                 "11110"
                 "10110"
                 "10111"
                 "10101"
                 "01111"
                 "00111"
                 "11100"
                 "10000"
                 "11001"
                 "00010"
                 "01010")))
    (let* ((input (mapcar (lambda (line) (parse-integer line :radix 2)) input))
           (gamma (get-gamma input 5))
           (epsilon (epsilon-from-gamma gamma)))
      (assert (= 22 (bits-to-num gamma)))
      (assert (= 9 (bits-to-num epsilon)))
      (assert (= 198 (power-consumption input 5)))
      (assert (= 23 (get-rating input 5 #'bit-criteria-oxygen)))
      (assert (= 10 (get-rating input 5 #'bit-criteria-co2)))
      (format t "test passed~%"))))
(test)
