(ql:quickload 'cl-utilities)

(defun read-input (filename)
  (with-open-file (stream filename) (read-line stream nil)))

(defun parse-input (input)
  (mapcar #'parse-integer (cl-utilities:split-sequence #\, input)))

(defun cost (dest input)
  (reduce #'+ (mapcar (lambda (cur) (abs (- cur dest))) input)))

(defun cheapest-cost (cost-fn input)
  (destructuring-bind (min max) (loop for n in input
                                      minimize n into min
                                      maximize n into max
                                      finally (return `(,min ,max)))
    (loop for dest from min to max minimizing (funcall cost-fn dest input))))

(format t "part1: ~a~%" (cheapest-cost #'cost (parse-input (read-input "day7.input"))))

(defun fixed-cost (dest input)
  (reduce #'+ (mapcar (lambda (cur) (loop for x from 1 to (abs (- cur dest)) sum x)) input)))

(format t "part2: ~a~%" (cheapest-cost #'fixed-cost (parse-input (read-input "day7.input"))))
